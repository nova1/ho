<?php
/**
 *
 * Inclui o tipo de post "Case"
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com.br/rhbarauna
 *
 **/

function register_cases_type() {

    $labels = array(
        'name'               => __( 'Cases' ),
        'singular_name'      => __( 'Cases' ),
        'menu_name'          => __( 'Cases' ),
        'name_admin_bar'     => __( 'Cases' ),
        'add_new'            => __( 'Adicionar novo' ),
        'add_new_item'       => __( 'Adicionar novo Case' ),
        'new_item'           => __( 'Novo Case' ),
        'edit_item'          => __( 'Editar Case' ),
        'view_item'          => __( 'Ver Case' ),
        'all_items'          => __( 'Todos os Cases' ),
        'search_items'       => __( 'Buscar Cases' ),
        'not_found'          => __( 'Nenhum Case encontrado.' ),
        'not_found_in_trash' => __( 'Nenhum Case encontrado na lixeira.' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => true,
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'case', $args );

    $labels_cats = array(
        'name'              => __( 'Categorias', TEXT_DOMAIN ),
        'singular_name'     => __( 'Categoria', TEXT_DOMAIN ),
        'search_items'      => __( 'Buscar Categorias' ),
        'all_items'         => __( 'Todas as Categorias' ),
        'parent_item'       => __( 'Categorias Pai' ),
        'parent_item_colon' => __( 'Categorias Pai:' ),
        'edit_item'         => __( 'Editar Categoria' ),
        'update_item'       => __( 'Atualizar Categoria' ),
        'add_new_item'      => __( 'Adicionar Nova Categoria' ),
        'new_item_name'     => __( 'Nome do Nova Categoria' ),
        'menu_name'         => __( 'Categorias' ),
    );

    $tax_args = array(
        'hierarchical'      => true,
        'labels'            => $labels_cats,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'cases' ),
    );

    register_taxonomy( 'cases', array( 'case' ), $tax_args );
}
add_action( 'init', 'register_cases_type' );