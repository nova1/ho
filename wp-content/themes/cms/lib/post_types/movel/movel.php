<?php
/**
 *
 * Inclui o tipo de post "Case"
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com.br/rhbarauna
 *
 **/

function register_movel_type() {

    $labels = array(
        'name'               => __( 'Móveis' ),
        'singular_name'      => __( 'Móveis' ),
        'menu_name'          => __( 'Móveis' ),
        'name_admin_bar'     => __( 'Móveis' ),
        'add_new'            => __( 'Adicionar novo' ),
        'add_new_item'       => __( 'Adicionar novo Móvel' ),
        'new_item'           => __( 'Novo Móvel' ),
        'edit_item'          => __( 'Editar Móvel' ),
        'view_item'          => __( 'Ver Móvel' ),
        'all_items'          => __( 'Todos os Móveis' ),
        'search_items'       => __( 'Buscar Móvel' ),
        'not_found'          => __( 'Nenhum Móvel encontrado.' ),
        'not_found_in_trash' => __( 'Nenhum Móvel encontrado na lixeira.' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => true,
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'movel', $args );

    $labels_cats = array(
        'name'              => __( 'Categorias', TEXT_DOMAIN ),
        'singular_name'     => __( 'Categoria', TEXT_DOMAIN ),
        'search_items'      => __( 'Buscar Categorias' ),
        'all_items'         => __( 'Todas as Categorias' ),
        'parent_item'       => __( 'Categorias Pai' ),
        'parent_item_colon' => __( 'Categorias Pai:' ),
        'edit_item'         => __( 'Editar Categoria' ),
        'update_item'       => __( 'Atualizar Categoria' ),
        'add_new_item'      => __( 'Adicionar Nova Categoria' ),
        'new_item_name'     => __( 'Nome do Nova Categoria' ),
        'menu_name'         => __( 'Categorias' ),
    );

    $tax_args = array(
        'hierarchical'      => true,
        'labels'            => $labels_cats,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'moveis' ),
    );

    register_taxonomy( 'linhas', array( 'movel' ), $tax_args );
}
add_action( 'init', 'register_movel_type' );