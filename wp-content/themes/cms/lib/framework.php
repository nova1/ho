<?php
/**
 *
 * Biblioteca de Funções e Requisições
 *
 *	Version: 1.0
 *	Author: InfinitoAG
 *	Author URI: http://www.infinitoag.com
 *
**/

	// customização do Admin
	require_once( get_template_directory().'/lib/admin.php' );

	// Walker do Menu para Bootstrap
	require_once( get_template_directory().'/lib/includes/WP_Bootstrap_Navwalker.php' );
	
	// Cases
	require_once( get_template_directory().'/lib/post_types/cases/case.php' );

	//Moveis
	require_once( get_template_directory().'/lib/post_types/movel/movel.php' );


function _t( $return = true ) {
		if( $return == false ) return get_bloginfo('template_url');
		else echo get_bloginfo('template_url');
	}

	function get_related_pages() {

		global $post;

		$current = $post->ID;
		$parent = $post->ID;
		if( $post->post_parent != 0 ) $parent = $post->post_parent;

		$parent_post = get_post($parent);
	
		$args = array(
			'posts_per_page'   => 99,			
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'post_type'        => 'page',
			'post_parent'      => $parent
		);
		$pages = get_posts( $args );
		if( count($pages) > 0 ): ?>

		<div class="widget"><ul id="related-pages" class="pages">
		
		<li class="parent">
			<a href="<?php echo get_permalink($parent_post->ID); ?>">
			<?php echo $parent_post->post_title; ?>				
			</a>			
		</li>

		<?php 
		$args = array(
			'posts_per_page'   => 99,			
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'post_type'        => 'page',
			'post_parent'      => $current
		);
		$child = get_posts( $args );
		if( count($child) > 0 ) : ?>
		<li class="current">
			<?php if( $post->post_parent != 0 ): ?>			
			<strong><?php echo $post->post_title; ?></strong>
			<?php endif; ?>
			
			<ul class="sub-menu">
				<?php foreach ( $child as $post ) : setup_postdata( $post ); ?>
				<?php if( get_field('exibir_nos_relacionados') != 'nao' ): ?>
				<li <?php if( $post->ID == $current ) echo 'class="current"'; ?>>
					<a href="<?php the_permalink(); ?>">
						<?php if( get_field('rotulo_relacionados') != '' ) echo the_field('rotulo_relacionados'); 
						else the_title(); ?>
					</a>
				</li>
				<?php endif; ?>
			<?php endforeach; ?>
			</ul>
		</li>
		<?php wp_reset_postdata(); else: ?>

		<?php foreach ( $pages as $post ) : setup_postdata( $post ); ?>
			<?php if( get_field('exibir_nos_relacionados') != 'nao' ): ?>
			<li <?php if( $post->ID == $current ) echo 'class="current"'; ?>>
				<a href="<?php the_permalink(); ?>">
					<?php if( get_field('rotulo_relacionados') != '' ) echo the_field('rotulo_relacionados'); 
					else the_title(); ?>
				</a>
			</li>
			<?php endif; ?>
		<?php endforeach; endif; ?>
		</ul></div>

		<?php endif; wp_reset_postdata();
	}

	// pagina de opções personalizada
	if( function_exists('acf_add_options_page') ) {	 
		$option_page = acf_add_options_page(array(
			'page_title' 	=> 'Opções do Site',
			'menu_title' 	=> 'Opções do Site',
			'menu_slug' 	=> 'site-options',
			'capability' 	=> 'manage_options',
			'redirect' 	=> false
		));
	 
	}

	// imprime a data formatada do post
	function get_post_formatted_date( $post_id ) {
		// $date = get_the_date( 'd/m/Y', $post_id ); 
		// return $date;

		$format = "d/m/Y";

		global $post;
	    $post = get_post( $post_id );
	    setup_postdata( $post );

	    $modified_time = get_the_date( $format, $post->ID );

	    wp_reset_postdata( $post );

	    return $modified_time;
	}
	
	function get_nome_mes( $mes_num ) {

		$mes_nome = 'Janeiro';

		if($mes_num == 1){
	    	$mes_nome = "Janeiro";
	    }elseif($mes_num == 2){
	    	$mes_nome = "Fevereiro";
	    }elseif($mes_num == 3){
	    	$mes_nome = "Março";
	    }elseif($mes_num == 4){
	    	$mes_nome = "Abril";
	    }elseif($mes_num == 5){
	    	$mes_nome = "Maio";
	    }elseif($mes_num == 6){
	    	$mes_nome = "Junho";
	    }elseif($mes_num == 7){
	    	$mes_nome = "Julho";
	    }elseif($mes_num == 8){
	    	$mes_nome = "Agosto";
	    }elseif($mes_num == 9){
	    	$mes_nome = "Setembro";
	    }elseif($mes_num == 10){
	    	$mes_nome = "Outubro";
	    }elseif($mes_num == 11){
	    	$mes_nome = "Novembro";
	    }else{
	    	$mes_nome = "Dezembro";
	    }

	    return $mes_nome;
	}
?>