<?php
/**
 *
 * Plug-in para customização de Admin
 *
 *  Version: 1.0
 *  Author: Agencia Vogal, Nova1
 *  Author URI: http://www.agenciavogal.com.br, http://www.infinitoag.com
 *
**/
/**
 * Customização da tela de login
**/
function redirect_login_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'redirect_login_url' );
function login_url_title() {
    return get_bloginfo( 'name' );
}
add_filter( 'login_headertitle', 'login_url_title' );
/**
 * Customização da admin bar
**/
function custom_admin_bar_css() {
    $css = '<style type="text/css">';
    if( current_user_can( 'activate_plugins' ) == false ) :
        //$css .= '#wp-admin-bar-wp-logo .ab-icon { background: url('.get_bloginfo('template_directory').'/img/logo-default.png) no-repeat center top !important; }';
        $css .= '.update-nag { display: none !important; }';
        $css .= '#add-product_cat { display: block !important; }';
        // $css .= '#add-category { display: none; }';
        // $css .= '#postimagediv, #authordiv { display: none; }';
    endif;
    $css .= '</style>';
    echo $css;
}
add_action('admin_head', 'custom_admin_bar_css');
/**
 * Customiza o logo de login
**/
function update_login_logo() {
    echo '
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url('.get_bloginfo( 'template_directory' ).'/img/logo_small.png);
            padding-bottom: 30px;
            background-size: contain;
            width: 90%;
        }
    </style>';
}
add_action( 'login_enqueue_scripts', 'update_login_logo' );
/**
 * Remover notificações e menus de contexto
**/
function wpmidia_remove_screen_options(){
    if ( current_user_can( 'activate_plugins' ) ) {
        return true;
    }
    else{
        return false;
    }
}
// add_filter( 'screen_options_show_screen', 'wpmidia_remove_screen_options' );

// remove update notice for forked plugins
function remove_update_notifications( $value ) {

    if ( isset( $value ) && is_object( $value ) ) {
        unset( $value->response[ 'advanced-custom-fields-pro/acf.php' ] );
    }

    return $value;
}
add_filter( 'site_transient_update_plugins', 'remove_update_notifications' );

/**
 * Customiza a mensagem do rodapé
**/
function remove_footer_admin () {
    echo '© <a href="http://www.infinitoag.com">Nova1</a>. Powered by Wordpress with Woocommerce.';
}
add_filter('admin_footer_text', 'remove_footer_admin');
?>
