<?php
/**
 *
 * Template name: Contato
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header(); ?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <article id="contato" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <h1 class="sr-only"><?php the_title(); ?></h1>
                    <div id="breadcrumbs">
                        <?php
                        if (function_exists('yoast_breadcrumb'))
                            yoast_breadcrumb();
                        ?>
                    </div>
                </div>
                <div class="row">
                    <h1 class="page-title">
                    </h1>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="page-subtitle">
                <?php the_field('texto-headline') ?>
            </div>
            <div id="page-content" class="marginless">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" id="tabs-contato" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="mensagem-tab" data-toggle="tab" href="#mensagem"
                                   role="tab" aria-controls="mensagem" aria-expanded="true">CONTATO / SAC</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="trabalhe-tab" data-toggle="tab" href="#trabalhe" role="tab"
                                   aria-controls="trabalhe" aria-expanded="true">TRABALHE CONOSCO</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="licitacoes-tab" data-toggle="tab" href="#licitacoes" role="tab"
                                   aria-controls="licitacoes" aria-expanded="true">LICITAÇÕES</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <p>&nbsp;</p>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="forms-area">
                            <div class="container">
                                <div class="col-lg-12">
                                    <div class="tab-content">
                                        <div class="form-tab-pane tab-pane fade show active" id="mensagem"
                                             role="tabpanel"
                                             aria-labelledby="mensagem-tab">
                                            <div class="row">
                                                <div class="col-lg-6" id="form-contato">
                                                    <?php the_field('texto_contato') ?>
                                                    <?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato"]') ?>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div id="page-sidebar" class="sidebar-right">
                                                        <?php the_field('conteudo_contato') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-tab-pane tab-pane fade" id="trabalhe" role="tabpanel"
                                             aria-labelledby="trabalhe-tab">
                                            <div class="row">
                                                <div class="col-lg-6" id="form-trabalhe-conosco">
                                                    <?php echo do_shortcode('[contact-form-7 id="96" title="Formulário Trabalhe Conosco"]') ?>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div id="page-sidebar" class="sidebar-right">
                                                        <?php the_field('conteudo_trabalhe_conosco') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-tab-pane tab-pane fade" id="licitacoes" role="tabpanel"
                                             aria-labelledby="licitacao-tab">
                                            <div class="row">
                                                <div class="col-lg-6" id="form-licitacoes">
                                                    <?php echo do_shortcode('[contact-form-7 id="84" title="Formulário de Licitações"]') ?>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div id="page-sidebar" class="sidebar-right">
                                                        <?php the_field('conteudo_licitacoes') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- fim do .content-body -->
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>