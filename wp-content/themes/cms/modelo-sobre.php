<?php
/**
 *
 * Template name: Sobre
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header(); ?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <article id="sobre" class="content">
        <header id="page-header">
            <div class="container">
                <h1 class="sr-only"><?php the_title();?></h1>
                <div class="row">
                    <div id="breadcrumbs">
                        <?php
                        if (function_exists('yoast_breadcrumb'))
                            yoast_breadcrumb();
                        ?>
                    </div>
                </div>
                <div class="row">
                    <h1 class="page-title">
                    </h1>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="page-subtitle">
                <?php the_field('texto-headline') ?>
            </div>
            <div class="container">
                <div id="page-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs" id="tabs-a-empresa" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="quem-somos-tab" data-toggle="tab" href="#quem-somos"
                                       role="tab" aria-controls="quem-somos" aria-expanded="true">QUEM SOMOS</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="showroom-tab" data-toggle="tab" href="#showroom" role="tab"
                                       aria-controls="showroom" aria-expanded="true">SHOWROOM</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="fabrica-tab" data-toggle="tab" href="#fabrica" role="tab"
                                       aria-controls="fabrica" aria-expanded="true">FÁBRICA</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <div class="col-md-12 mr-auto ml-auto">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="quem-somos" role="tabpanel"
                                     aria-labelledby="quem-somos-tab">
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto centered-container">
                                            <div class="tab-text-img centered-item">
                                                <?php the_field('quem_somos_texto_banner') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <?php if (get_field('quem_somos_banner')): ?>
                                                <img src="<?php the_field('quem_somos_banner') ?>" alt="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade show" id="showroom" role="tabpanel"
                                     aria-labelledby="showroom-tab">
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto centered-container">
                                            <div class="tab-text-img centered-item">
                                                <?php the_field('showroom_texto_banner') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <?php if (get_field('showroom_banner')): ?>
                                                <img src="<?php the_field('showroom_banner') ?>" alt="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade show" id="fabrica" role="tabpanel"
                                     aria-labelledby="fabrica-tab">
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto  centered-container">
                                            <div class="tab-text-img centered-item">
                                                <?php the_field('fabrica_texto_banner') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <?php if (get_field('fabrica_banner')): ?>
                                                <img src="<?php the_field('fabrica_banner') ?>" alt="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- fim do .content-body -->
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>