<?php
/**
 * Template dos comentários
 * Exibe comentários do Facebook para a URL
 *
 * Version: 1.0
 * Author: Agencia Vogal, Nova1
 * Author URI: http://www.agenciavogal.com.br, http://www.infinitoag.com
 *
**/

// Protege o arquivo contra acessos diretos
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Please do not load this page directly. Thanks!');

// Caso o post seja protegido por senha
if ( post_password_required() ) :
    echo '<p class="nocomments">Este artigo está protegido por password. Insira-a para ver os comentários.</p>';
    return;
endif;

// Caso o post tenha comentários, inclui o template de comentários do Facebook
if ( comments_open() ) : ?>
<div id="comments">          
    <h3>Comentários</h3>   
    
    <?php /*    
    <ul>
    <?php 
    $args = array( 'reply_text' => 'Responder' ); 
    wp_list_comments( $args );
    ?>
    </ul>

    <?php 
    $args = array( 'comment_notes_after' => "", 'comment_notes_before' => '' );
    comment_form( $args ); */
    ?>

</div><!-- fim do .comments -->
<?php endif; ?>