<?php
/**
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com/rhbarauna
 *
 **/
get_header(); ?>

<article id="posts" class="content">
    <header id="page-header">
        <div class="container">
            <h1 class="sr-only"><?php the_title();?></h1>
            <div class="row">
                <div id="breadcrumbs" class="last-item-as-title">
                    <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb(); ?>
                </div>
            </div>
        </div>
    </header>
    <section class="content">
        <div class="container">
            <div id="page-content">
                <?php
                $paged = get_query_var('paged');
                $query_args = array(
                    'posts_per_page' => 4,
                    'post_status' => 'publish',
                    'post_type' => 'post',
                    'orderby' => 'modified',
                    'order' => 'DESC',
                    'paged'=> $paged
                );

                $query = new WP_Query($query_args);

                if ($query->have_posts()):
                    $query->the_post();
                    unset($query->posts[0]);
                    ?>
                    <a href="<?php the_permalink(); ?>">
                        <div class="row">
                            <div class="col-lg-12" id="blog-image">
                                <img src="<?php the_field("sample_image"); ?>">
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">
                            <div class="col-lg-12" id="blog-image-bottom-text">
                                <?php the_title(); ?>
                            </div>
                        </div>
                    </a>
                    <p>&nbsp;</p>
                    <div class="img-gallery">
                        <div class="row">
                            <?php
                            while ($query->have_posts()): $query->the_post(); ?>
                                <a href="<?php the_permalink(); ?>" class="col-sm-4 img-gallery-item">
                                    <div class="thumb-wrapper">
                                        <div class="thumb thumb-text">
                                            <div class="thumb-text-content">
                                                <h2><?php the_title(); ?></h2>
                                                <div class="space"></div>
                                                <?php the_excerpt() ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php
                    if (function_exists('wp_pagenavi')): // if PageNavi is activated
                        wp_pagenavi( array('query'=>$query)); // Use PageNavi
                    endif;
                endif; ?>
            </div>
            <p>&nbsp;</p>
        </div>
    </section><!-- fim do .content-body -->
</article><!-- fim do .content -->

<?php get_footer(); ?>