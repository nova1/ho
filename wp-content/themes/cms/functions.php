<?php
/**
 *
 * Funções e Definições para o tema
 *
 *	Version: 1.0
 *	Author: InfinitoAG
 *	Author URI: http://www.infinitoag.com
 *
**/
// Registra folhas de estilo
function setup_theme_styles() {
	// Primeiro Registra	
	wp_register_style( 'bootstrap', "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" );
	wp_register_style( 'font1', 'https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700,700i' );		
	wp_register_style( 'fa', get_template_directory_uri().'/css/font-awesome.min.css' );
	wp_register_style( 'style', get_template_directory_uri().'/style.css' );

	// Depois adiciona na fila
	wp_enqueue_style( 'bootstrap' );
	wp_enqueue_style( 'font1' );
	wp_enqueue_style( 'fa' );
	wp_enqueue_style( 'style' );
}
add_action( 'wp_enqueue_scripts', 'setup_theme_styles' );

// Registra arquivos de scripts
function setup_theme_scripts() {

	wp_register_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'imagesLoaded', get_template_directory_uri().'/js/jquery.imagesLoaded.min.js', array( 'jquery' ), false, true );	
	wp_register_script( 'scripts', get_template_directory_uri().'/js/scripts.js', array( 'jquery' ), false, true );
	
	wp_localize_script( 'scripts', 'config', array( 
		'ajax_url' => admin_url( 'admin-ajax.php' ), 
		'template_url' => get_bloginfo( 'template_url' )  
	));

	// Depois adiciona na fila
	wp_enqueue_script( 'popper' );
	wp_enqueue_script( 'bootstrap' );
	wp_enqueue_script( 'imagesLoaded' );
	wp_enqueue_script( 'scripts' );
}
add_action( 'wp_enqueue_scripts', 'setup_theme_scripts' );

// Tamanhos das Imagens Destacadas
add_theme_support( 'post-thumbnails' );

// Registra menus de Navegação
register_nav_menus( array(
	'menu_topo' => 'Menu Topo',
	'full_menu' => 'Menu Completo'	
));

 register_sidebar( array(
 	'name'          => 'Padrão',
 	'id'            => 'default',
 	'description'   => 'Área de widgets Padrão do site',
 	'before_widget' => '<div id="%1$s" class="widget %2$s">',
 	'after_widget'  => '</div>',
 	'before_title'  => '<h3 class="widgettitle">',
 	'after_title'   => '</h3>'
 ));

// Inclui arquivo de Shortcodes
require_once( get_template_directory().'/lib/framework.php' );

?>