<?php
/**
 *
 * Sidebar
 * Modelo padrão de barra lateral
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/



?>
<div id="sidebar">
   <ul>
      <?php
      if ( function_exists('dynamic_sidebar') && dynamic_sidebar('default') ) :
          the_field('blog_sidebar_content', 'option');
      endif; ?>
</ul>
</div>