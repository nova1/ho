<?php
/**
 *
 * Template Single
 * Página padrão para detalhe do post
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header();
$current_id = get_the_ID();
global $post;
?>
<?php if (have_posts()): while (have_posts()): the_post();
    ?>
    <article id="single" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <div id="breadcrumbs" class="last-item-as-title">
                        <?php
                        if (function_exists('yoast_breadcrumb'))
                            yoast_breadcrumb();
                        ?>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="page-subtitle">
                <?php
                the_excerpt()
                ?>
            </div>
            <div id="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div id="page-content" class="blog-content">
                                <?php the_field("texto_postagem"); ?>
                            </div>
                        </div>

                        <div class="col-md-3 blog-sidebar">
                            <?php get_sidebar('default'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <h2 class="text-center">
                                Veja também
                            </h2>

                            <p>&nbsp;</p>
                            <div class="img-gallery">

                                <div class="row">
                                    <?php
                                    $query_args = array(
                                        'posts_per_page' => 3,
                                        'post_status' => 'publish',
                                        'post_type' => 'post',
                                        'orderby' => 'modified',
                                        'order' => 'DESC',
                                        'post__not_in' => array($current_id)
                                    );

                                    $query = new WP_Query($query_args);

                                    if ($query->have_posts()):
                                        while ($query->have_posts()): $query->the_post();
                                            ?>
                                            <a href="<?php the_permalink(); ?>" class="col img-gallery-item">
                                                <div class="thumb-wrapper">
                                                    <div class="thumb thumb-text">
                                                        <div class="thumb-text-content">
                                                            <h2><?php the_title(); ?></h2>
                                                            <div class="space"></div>
                                                            <?php the_excerpt() ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php endwhile;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
