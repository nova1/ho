<?php
/**
 * Template do Rodapé
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
?>

</div><!-- /.main -->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h5 class="title-separator">Marcas que já atendemos</h5>
            </div>
        </div>
        <div id="galeria-clientes">
            <?php
            $clients = get_field('galeria_clientes', 'option');
            $size = 'full';
            if ($clients):
                foreach ($clients as $client):?>
                    <div class="logo-cliente">
                        <?php
                        echo wp_get_attachment_image($client['ID'], $size); ?>
                    </div>
                <?php endforeach;
            endif;
            ?>
        </div><!-- /.row -->
        <hr class="separator">
        <div class="row">
            <div class="col-lg-2">
                <div id="footer-logo">
                    <a href="<?php bloginfo('url') ?>" title="<?php bloginfo('name') ?>">
                        <img src="<?php bloginfo('template_url'); ?>/img/Logo_rodape.png"
                             alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
            </div>

            <div class="col-lg-10 d-none d-sm-block">
                <div class="social">
                    <?php if (get_field('facebook', 'option')): ?>
                        <a href="<?php the_field('facebook', 'option') ?>" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>

                    <?php if (get_field('linkedin', 'option')): ?>
                        <a href="<?php the_field('linkedin', 'option') ?>" target="_blank">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                </div>
                <nav id="bottom-menu" role="navigation">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'full_menu',
                        'menu_class' => 'nav nav-pills',
                        'container' => false,
                        'walker' => new wp_bootstrap_navwalker()
                    )); ?>
                </nav>

            </div>

        </div><!-- /.row -->
    </div><!-- /.container -->

    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-copyright-text">
                    <small>
                        &copy;
                        <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. Todos os Direitos Reservados.
                    </small>
                </div>
                <div class="col-md-6 col-copyright-logo">
                    <img src="<?php bloginfo('template_url'); ?>/img/Marca_Neocom.png"/>
                </div>
            </div>
        </div>
    </div>
</footer><!-- end #footer -->

<?php wp_footer(); ?>
</body>
</html>