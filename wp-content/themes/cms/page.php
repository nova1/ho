<?php
/**
 *
 * Template de Página Padrão
 * Página com conteúdo na esquerda e barra lateral na direira
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
get_header(); 
if( have_posts() ): while( have_posts() ): the_post(); ?>

	<article  class="content">
		<header id="page-header" <?php if( get_field('cor_destaque') ) echo 'style="background-color: '.get_field('cor_destaque').';"' ?>>
			<div class="container">
				<div class="row">
                    <div class="col-md-5">
                        <div id="breadcrumbs">
                            <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb(); ?>
                        </div>
                    </div>

					<div class="col-md-7">
						<h1 class="page-title">
							<?php the_title(); ?>
						</h1>						
					</div>
				</div>
			</div>
		</header>

		<div class="container">
			<div id="compartilhar"><?php echo do_shortcode('[ssba]'); ?></div>
			<div class="clearfix"></div>
		</div>		

		<section class="content">
			<div class="container">
				<div class="row">

					<?php if( get_field('layout') == 'left_sidebar' ): ?>
					<div class="col-md-3">
						<div id="sidebar-toggle">
							<a role="button" data-toggle="collapse" href="#sidebar-collapse" aria-expanded="false" aria-controls="sidebar-collapse">
								Navegue nessa página <i class="fa fa-chevron-down" aria-hidden="true"></i>
							</a>
						</div>
						
						<div id="sidebar-collapse" class="collapse">
							<div id="page-sidebar" class="sidebar-left">
							<?php 
							if( has_post_thumbnail() ) the_post_thumbnail();

							// mostrar páginas relacionadas
							if( get_field('usar_paginas_relacionadas') == 'sim') get_related_pages(); 
							
							// usar sidebar
							if( get_field('usar_sidebar') == 'sim' ) {
								 if( get_field('sidebar') ) dynamic_sidebar( get_field('sidebar') );
							}

							// html customizado
							if( get_field('html_customizado') ) the_field('html_customizado');
							?>
							</div>
						</div>
					</div>
					<?php endif; ?>

					<div class="col-md-<?php if( get_field('layout') == 'full' ) echo '12'; else echo '9'; ?>">
						<div id="page-content">
							<?php the_content(); ?>
						</div>						
					</div>

					<?php if( get_field('layout') == 'right_sidebar' ): ?>
					<div class="col-md-3">
						<div id="sidebar-toggle">
							<a role="button" data-toggle="collapse" href="#sidebar-collapse" aria-expanded="false" aria-controls="sidebar-collapse">
								Navegue nessa página <i class="fa fa-chevron-down" aria-hidden="true"></i>
							</a>
						</div>

						<div id="sidebar-collapse" class="collapse">
							<div id="page-sidebar" class="sidebar-right">
								<?php 
								if( has_post_thumbnail() ) the_post_thumbnail();

								// mostrar páginas relacionadas
								if( get_field('usar_paginas_relacionadas') == 'sim') get_related_pages(); 
								
								// usar sidebar
								if( get_field('usar_sidebar') == 'sim' ) {
									 if( get_field('sidebar') ) dynamic_sidebar( get_field('sidebar') );
								}

								// html customizado
								if( get_field('html_customizado') ) the_field('html_customizado');
								?>
							</div>
						</div>
					</div>
					<?php endif; ?>

				</div>
			</div>
		</section><!-- fim do .content-body -->
		<?php endwhile; endif; ?>
	</article><!-- fim do .content -->	
<?php get_footer(); ?>