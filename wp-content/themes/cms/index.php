<?php
/**
 *
 * Template Index/Modelo
 * Página padrão para listagem de posts
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
get_header(); 
?>
	<article id="blog" class="content">
		<header id="page-header">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<h1 class="page-title">
							<?php if( is_category() ): ?>
							<?php single_cat_title(); ?>
							<?php elseif( is_search() ): ?>
							Resultados da busca: <?php echo $_GET['s']; ?>
							<?php else: ?>
							Publicações
							<?php endif; ?>
						</h1>						
					</div>

					<div class="col-md-5">
						<div id="breadcrumbs">
						<?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb(); ?>
						</div>
					</div>
				</div>
			</div>
		</header>

		<section class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-8">

						<?php if( is_category() ){
							$cat = get_queried_object();
							$cat_id = $cat->term_id;

							if( $cat_id == 5 or $cat_id == 12 ):
								$ano = isset($_GET['ano']) ? $_GET['ano'] : false;

								if( !$ano ): ?>
									<h2><?php echo $cat->name; ?> por ano:</h2>
									<p>Clique em um ano para acessar o arquivo:</p>
									<p>&nbsp;</p>
									<?php
									$min_ano = 2003;
									if( $cat_id == 5 ) $min_ano = 2008;

									$max_ano = date('Y');

									for ($i=$max_ano; $i >= $min_ano; $i--) :
										$url = get_category_link($cat_id).'?ano='.$i;
										echo '<p class="link-arquivo"><a href="'.$url.'">'.$i.'</a></p>';
									endfor; ?>

									<p>&nbsp;</p>
									<p>&nbsp;</p>

								<?php else : ?>

									<h2><?php echo $cat->name.': '.$ano; ?></h2>
									<p class="arquivos-voltar">
										<a href="<?php echo get_category_link($cat_id); ?>">Voltar para todos os anos</a>
									</p>

									<p>&nbsp;</p>

									<?php
									for ($j=12; $j >= 1; $j--):
										$args = array(
											'posts_per_page'   => 99,
											'category'         => $cat_id,
											'orderby'          => 'date',
											'order'            => 'DESC',
											'year'				=> $ano,
											'monthnum'			=> $j
										);

										$posts = get_posts( $args );
										if( count($posts) > 0 ): ?>

											<div class="arquivos-mes">
												<h3 class="nome-mes">
													<?php echo get_nome_mes($j); ?>
												</h3>

												<div class="arquivos-posts">
												<?php foreach ($posts as $post): setup_postdata($post); ?>
													<div class="arquivo-post">
														<span class="arquivo-post-data"><?php the_date(); ?></span>

														<a href="<?php the_permalink(); ?>">
															<?php the_title(); ?><br>
														</a>
													</div>

												<?php endforeach; ?>
												</div>
											</div>
										<?php endif;
									endfor;
								endif;
							else:

							if ( have_posts() ): while ( have_posts() ): the_post();
							assert( "locate_template( array('templates/post_item.php', 'post_item.php'), true, false )" );
							endwhile; endif;

							wp_pagenavi();

							endif;
						} else { ?>
							<?php if ( have_posts() ): while ( have_posts() ): the_post();
							assert( "locate_template( array('templates/post_item.php', 'post_item.php'), true, false )" );
							endwhile; endif; ?>

							<?php wp_pagenavi(); ?>
						<?php } ?>
					</div>

					<div class="col-md-4">
						<div id="page-sidebar" class="sidebar-right">
						<?php dynamic_sidebar('default'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>		
	</article><!-- fim do .content -->
<?php get_footer(); ?>
