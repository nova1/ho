<?php
/**
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com/rhbarauna
 *
 **/
get_header(); ?>

    <article id="posts" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <div id="breadcrumbs" class="last-item-as-title">
                        <?php if (function_exists('yoast_breadcrumb')) yoast_breadcrumb(); ?>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="page-subtitle">
                <?php the_field('products_headline', 'option'); ?>
            </div>
            <div class="container">

                <div id="page-content" class="img-gallery">
                    <div class="row">
                        <?php
                        $query_args = array(
                            'posts_per_page' => 12,
                            'post_status' => 'publish',
                            'post_type' => 'movel',
                            'orderby' => 'modified',
                            'order' => 'DESC'
                        );

                        $query = new WP_Query($query_args);

                        if (have_posts()):
                            while (have_posts()): the_post();
                                ?>
                                <a href="<?php the_permalink(); ?>" class="col-sm-4 img-gallery-item clickable">
                                    <div class="thumb-wrapper">
                                        <div class=" thumb">
                                            <?php the_post_thumbnail('full') ?>
                                            <div class="link">
                                                <div>
                                                    <p>Conheça</p>
                                                    <p class="arrow"> &#8594;</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </section><!-- fim do .content-body -->
    </article><!-- fim do .content -->
<?php get_footer(); ?>