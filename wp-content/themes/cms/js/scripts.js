/**
 * Scripts de experiência front-end
 *
 * Autor: Infinito Web Sites
 * URL do Autor: http://www.infinito.com.br
 **/
jQuery(document).ready(function ($) {

    // imagesLoaded( 'body', function() {
    // if( window.innerWidth > 900 ){
    //     $('#sidebar-collapse').addClass('in');
    // } else {
    //     $( '#sidebar-toggle a' ).click(function(event) {
    //         if( $( '#sidebar-toggle a .fa' ).hasClass('fa-chevron-down') ) {
    //             $( '#sidebar-toggle a .fa' ).removeClass('fa-chevron-down').addClass('fa-chevron-up');
    //         } else {
    //             $( '#sidebar-toggle a .fa' ).removeClass('fa-chevron-up').addClass('fa-chevron-down');
    //         }
    //     });
    // }

    // utils

    function adjustGalleryThumbs(init) {
        console.log('adjustGalleryThumbs');
        var $imgs = $(".img-gallery .thumb");
        for (var i = 0; i < $imgs.length; i++) {
            var $img = $($imgs[i]);
            if ($img.width() > 0) {
                $img.height($img.width());
                if (init) {
                    $img.css('opacity', 1);
                }
            }
        }
    }

    window.onscroll = function () {
        if (is_home() && window.scrollY > 75) {
            $('body').addClass('scrolled');
        } else {
            $('body').removeClass('scrolled');
        }
    };

    $('#loading').fadeOut(400);

    $("article.content .nav-item").click(function () {
        var elementText = $(this).text().trim();
        showTitle(elementText, true);
    });

    function showTitle(title, replaceCurrentTxt) {
        if (replaceCurrentTxt === undefined) {
            replaceCurrentTxt = true;
        }
        var $elem = $("article.content header .page-title");
        if (!replaceCurrentTxt && $.trim($elem.text()).length > 0) {
            return;
        }
        $elem.html(title)
    }

    var firstTitle = $("article.content .nav-item .active").text().trim();

    showTitle(firstTitle, false);

    // auto resize img thumb to maintain the proportion
    setTimeout(function () {
        adjustGalleryThumbs(true);
    }, 100);

    $(window).resize(function () {
        adjustGalleryThumbs();
    })


});

// detecta se é uma determinada página
function is_page(p) {
    var url = window.location.toString();
    if (((url.indexOf(p) > 0))) return true;
    return false;
}

// detecta se é a home do site
function is_home() {
    var url = window.location.toString();
    if ((url.indexOf('home') > 0) || (url.indexOf('?page_id=') < 0)) return true;
    return false;
}

function resize_home_slider() {
    var wh = window.innerHeight; // tela - menu
    jQuery('#home-slider .item>div').css('height', wh);
}
