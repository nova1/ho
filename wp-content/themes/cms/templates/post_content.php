<?php
/**
 *
 * Template para exibição de post único
 *
 * Version: 1.0
 * Author: Infinito Web Sites
 *
**/

	$post_date = array(
		'day' => get_the_date('d'),
		'month' => get_the_date('m'),
		'month_name' => get_the_date('M'),
		'year' => get_the_date('Y')
	); 

	$unique_date = $post_date['day'] . '/' . $post_date['month'] . '/' . $post_date['year'];
	?>
			
	<article class="post post-item">
		<a href="<?php the_permalink(); ?>">
			<div class="thumb text-center">
				<?php if( has_post_thumbnail() ) the_post_thumbnail( 'full' ); ?>
			</div>
		</a>

		<div class="title">
			<a href="<?php the_permalink(); ?>">
				<h1><?php the_title(); ?></h1>
			</a>
		</div>		

		<div class="post-header-meta">
			<span>
				<?php echo $post_date['day'] . '/' . $post_date['month'] . '/' . $post_date['year']; ?> /
				Em: <?php the_category(', '); ?>
			</span>
		</div>		

		<p>&nbsp;</p>		

		<div class="content">				
			<?php the_content(); ?>
		</div>	

		<p>&nbsp;</p>

		<!-- <section class="post-comments">
			<?php /* comments_template(); */?>
		</section> -->
					
	</article>