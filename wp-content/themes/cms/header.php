<?php
/**
 * Template do Cabeçalho
 * Exibe tudo na seção <head> e início do <body> até o <article>
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- monta o titulo da página -->

    <title><?php wp_title(); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Vidaloka" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="loading"></div>

<nav id="header" class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img id="logo-header" src="<?php bloginfo('template_url'); ?>/img/logo_topo.png"
                 alt="<?php bloginfo('name'); ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!--            <ul class="navbar-nav mr-auto">-->
            <?php wp_nav_menu(array(
                'theme_location' => 'menu_topo',
                'menu_class' => 'navbar-nav ml-auto',
                'container' => false,
                'walker' => new wp_bootstrap_navwalker()
            )); ?>
            <div class="col" id="header-social-wrapper">
                <div id="header-social">
                    <div class="social">
                        <?php if (get_field('facebook', 'option')): ?>
                            <a href="<?php the_field('facebook', 'option') ?>" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (get_field('linkedin', 'option')): ?>
                            <a href="<?php the_field('linkedin', 'option') ?>" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-1" id="header-search-btn">
                <div data-toggle="modal" data-target="#searchModal">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</nav>

<div class="main">
    <div class="modal" tabindex="-1" role="dialog" id="searchModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
                </div>
            </div>
        </div>
    </div>


					