<?php
/**
 *
 * Template do Formulário de Buscas
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
?>
<form id="searchform" class="form-inline" method="get" role="search" action="<?php echo home_url('/'); ?>">
	<div class="form-group">
		<label class="sr-only" for="s">Search</label>
		<input type="text" class="form-control" id="s" name="s" placeholder="Procure no site">
	</div>
	<button type="submit" id="searchsubmit">
		<i class="fa fa-search" aria-hidden="true"></i>
	</button>
</form>