<?php
/**
 *
 * Template name: Home
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header(); ?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <article id="home" class="content">
        <h1 class="sr-only"><?php the_title();?></h1>
        <div id="slider-row">
            <a href="<?php echo home_url(); ?>" id="home-logo-link" style="background-image: <?php the_field('logotipo', 'option'); ?>" alt="<?php bloginfo('name'); ?>"></a>
            <div id="home_slider">
                <div class="container">
                    <a href="<?php echo home_url(); ?>" id="home-logo-link">
                        <img src="<?php the_field('logotipo', 'option'); ?>" alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
                <?php if (function_exists('cyclone_slider')) cyclone_slider('home'); ?>
            </div>
        </div>
        <div id="home-about-row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 home-about-text">
                        <?php the_field('home-about'); ?>
                    </div>
                    <div class="col-lg-4">
                        <hr class="blue-line-about-text">
                    </div>
                </div>
            </div>
        </div>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <section class="img-gallery">
            <div class="row">
                <?php
                $query_args = array(
                    'posts_per_page' => 8,
                    'post_status' => 'publish',
                    'post_type' => 'movel',
                    'orderby' => 'modified',
                    'order' => 'DESC',
                    'include' => $moveis_home
                );

                $query = new WP_Query($query_args);

                if ($query->have_posts()):
                    while ($query->have_posts()):
                        $query->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="col-sm-3 img-gallery-item clickable">
                            <div class="thumb-wrapper">
                                <div class=" thumb">
                                    <?php the_post_thumbnail('full') ?>
                                    <div class="link">
                                        <div>
                                            <p>Saiba mais</p>
                                            <p class="arrow"> &#8594;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php endwhile; endif; ?>
            </div>
        </section>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <div class="container blog-list-area">
            <div class="row">
                <div class="col-3">
                    <p class="home-blog-title">Blog</p>
                    <hr class="separator separator-cyan">
                </div>
            </div>

            <div id="blog-list">
                <div class="row">
                    <?php
                    $query_args = array(
                        'posts_per_page' => 4,
                        'post_status' => 'publish',
                        'post_type' => 'post',
                        'orderby' => 'modified',
                        'order' => 'DESC'
                    );

                    $query = new WP_Query($query_args);

                    if ($query->have_posts()):
                        while ($query->have_posts()):
                            $query->the_post();
                            ?>
                            <div class="blog-title col-lg-3">
                                <a href="<?php the_permalink(); ?>">
                                    <?php
                                    the_title() ?>
                                </a>
                            </div>
                        <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        </section><!-- fim do .content-body -->
    </article>
<?php endwhile; endif; ?>
<?php get_footer(); ?>