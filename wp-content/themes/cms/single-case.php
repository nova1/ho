<?php
/**
 *
 * Template Single
 * Página padrão para detalhe do post
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header();
$current_id = get_the_ID();
?>
<?php if (have_posts()): while (have_posts()): the_post(); ?>

    <article id="single" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div id="breadcrumbs" class="last-item-as-title">
                            <?php
                            if (function_exists('yoast_breadcrumb'))
                                yoast_breadcrumb();
                            ?>
                        </div>
                    </div>
                </div>
        </header>

        <section class="content">
            <div class="page-subtitle">
                <?php
                the_field('texto_em_destaque');
                ?>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mr-auto ml-auto centered-container">
                        <div class="tab-text-right-img centered-item full-width">
                            <?php the_field('detalhes'); ?>
                        </div>
                    </div>

                    <div class="col-md-6 mr-auto ml-auto post-main-thumb">
                        <?php the_post_thumbnail(); ?>
                    </div>
                </div>
            </div>

            <div class="space"></div>
            <div class="space"></div>
            <div class="space"></div>

            <h2 class="text-center">Outras fotos do projeto</h2>

            <div class="space"></div>

            <div class="img-gallery">
                <?php
                if (get_field('lista_de_imagens')):
                    $images = get_field('lista_de_imagens'); ?>

                    <div class="row">
                        <?php foreach ($images as $image): ?>
                            <div class="col-sm-4 img-gallery-item">
                                <div class="thumb-wrapper">
                                    <div class=" thumb">
                                        <?php echo wp_get_attachment_image($image['ID']); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="space"></div>
            <div class="space"></div>
            <div class="space"></div>

            <p class="text-center">
                <a href="<?php echo home_url('/contato'); ?>" class="btn-action-blue"> Faça um
                    orçamento</a>
            </p>

            <div class="space"></div>
            <div class="space"></div>
            <div class="space"></div>


            <div class="container">
                <?php
                $query_args = array(
                    'posts_per_page'    => 3,
                    'post_status'       => 'publish',
                    'post_type'         => 'case',
                    'orderby'			=> 'modified',
                    'order'				=> 'DESC',
                    'post__not_in'      => array($current_id)
                );

                $query = new WP_Query($query_args);

                if( $query->have_posts() ):?>
                    <h2 class="text-center"> Conheça outros cases da HomeOffice </h2>

                    <div class="space"></div>

                    <div class="row">
                        <div class="img-gallery">
                            <div class="row">
                                <?php while( $query->have_posts() ): $query->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>" class="col img-gallery-item clickable">
                                        <div class="thumb-wrapper">
                                            <div class=" thumb">
                                                <?php the_post_thumbnail() ?>
                                                <div class="link">
                                                    <div>
                                                        <p>Conheça</p>
                                                        <p class="arrow"> &#8594;</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
    <div class="space"></div>
    <div class="space"></div>
<?php get_footer(); ?>