<?php
/**
 *
 * Template Single
 * Página padrão para detalhe do post
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header();
$current_id = get_the_ID();
?>
<?php if (have_posts()): while (have_posts()): the_post(); ?>

    <article id="single" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div id="breadcrumbs" class="last-item-as-title">
                            <?php
                            if (function_exists('yoast_breadcrumb'))
                                yoast_breadcrumb();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="page-subtitle">
                <?php
                the_field('texto_em_destaque');
                ?>
            </div>
            <?php
            $repeaterName = 'repetidor_movel';
            $rows = get_field($repeaterName); // get all the rows
            if (have_rows($repeaterName)):
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="container">
                            <ul class="nav nav-tabs" id="tabs" role="tablist">
                                <?php
                                $specific_row = $rows[0];
                                // loop through the rows of data
                                while (have_rows($repeaterName)) : the_row();
                                    $tab_label = get_sub_field('tab_label');
                                    $tab_label_lower = strtolower($tab_label);
                                    $active = '';
                                    if (strcmp($specific_row['tab_label'], $tab_label) == 0):$active = 'active'; endif;
                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php echo($active); ?>"
                                           id="<?php echo($tab_label_lower); ?>-tab" data-toggle="tab"
                                           href="#<?php echo($tab_label_lower); ?>" role="tab"
                                           aria-controls="<?php echo($tab_label_lower); ?>" aria-expanded="true">
                                            <?php echo($tab_label); ?>
                                        </a>
                                    </li>
                                <?php
                                endwhile;
                                ?>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mr-auto ml-auto">
                                <div class="tab-content">

                                    <?php
                                    // loop through the rows of data
                                    while (have_rows($repeaterName)) : the_row();
                                        $tab_label = get_sub_field('tab_label');
                                        $tab_label_lower = strtolower($tab_label);
                                        $active = '';
                                        if (strcmp($specific_row['tab_label'], $tab_label) == 0):$active = 'active'; endif;
                                        ?>
                                        <div class="tab-pane fade show <?php echo($active); ?>"
                                             id="<?php echo($tab_label_lower); ?>" role="tabpanel"
                                             aria-labelledby="<?php echo($tab_label_lower); ?>-tab">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6 mr-auto ml-auto centered-container">
                                                        <div class="tab-text-right-img centered-item full-width">
                                                            <?php the_sub_field('detalhes_da_linha_do_movel'); ?>
                                                            <div class="space"></div>

                                                            <a href="#" title="Clique para baixar o manual"
                                                               class="btn btn-clean pull-right">
                                                                Baixe o manual do produto <i
                                                                        class="fa fa-download"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mr-auto ml-auto post-main-thumb">
                                                        <?php if (get_sub_field('foto_linha_movel')):
                                                            $images = get_sub_field('foto_linha_movel');
                                                            ?>
                                                            <?php echo wp_get_attachment_image($images[0]['ID']); ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="space"></div>
                                            <div class="space"></div>
                                            <div class="space"></div>

                                            <h2 class="text-center">Outras fotos da linha</h2>

                                            <div class="space"></div>

                                            <div class="img-gallery">
                                                <div class="row">
                                                    <?php
                                                    unset($images[0]);
                                                    foreach ($images as $image): ?>
                                                        <div class="col-sm-4 img-gallery-item">
                                                            <div class="thumb-wrapper">
                                                                <div class=" thumb">
                                                                    <?php echo wp_get_attachment_image($image['ID']); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endif;
            ?>

            <div class="space"></div>
            <div class="space"></div>
            <div class="space"></div>

            <p class="text-center">
                <a href="<?php echo home_url('/contato'); ?>" class="btn-action-blue"> Faça um
                    orçamento</a>
            </p>

            <div class="space"></div>

            <div class="container">
                <hr>
                <div class="space"></div>

                <h2 class="text-center">Conheça outros de nossos produtos</h2>
                <div class="space"></div>

                <div class="row">
                    <?php
                    $query_args = array(
                        'posts_per_page' => 3,
                        'post_status' => 'publish',
                        'post_type' => 'movel',
                        'orderby' => 'modified',
                        'order' => 'DESC',
                        'post__not_in' => array($current_id)
                    );

                    $query = new WP_Query($query_args);

                    if ($query->have_posts()):
                        ?>
                        <div class="row">
                            <div class="img-gallery">
                                <div class="row">
                                    <?php while ($query->have_posts()): $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>"
                                           class="col img-gallery-item clickable">
                                            <div class="thumb-wrapper">
                                                <div class=" thumb">
                                                    <?php the_post_thumbnail() ?>
                                                    <div class="link">
                                                        <div>
                                                            <p>Conheça</p>
                                                            <p class="arrow"> &#8594;</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
